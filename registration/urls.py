from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from registration import views


urlpatterns = [
    url(r'^registrations/$', views.RegistrationList.as_view(),name='registration-list'),
    url(r'^registrations/(?P<pk>[0-9]+)/$',views.RegistrationDetail.as_view(),name='registration-detail'),
]
