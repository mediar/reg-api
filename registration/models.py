from django.db import models
from users.models import User

class Registration(models.Model):

    created = models.DateTimeField(auto_now_add=True)
    first_name = models.CharField(max_length=100, blank=False,null=False)
    last_name = models.CharField(max_length=100, blank=False,null=False)
    addr = models.CharField(max_length=100, blank=False,null=False)
    city = models.CharField(max_length=100, blank=False,null=False)
    plz = models.IntegerField(blank = False, null=False)
    phone_number = models.IntegerField(blank = False, null=False)
    email = models.CharField(max_length=100, unique=True,blank=False,null=False)

    first_reg_bool = models.BooleanField()

    tax_id = models.CharField(max_length=100,blank=True,null=True)
    social_id = models.CharField(max_length=100,blank=True,null=True)

    birth_date_str = models.DateField(blank=True,null=True)
    birth_place = models.CharField(max_length=100,blank=True,null=True)
    birth_name  = models.CharField(max_length=100,blank=True,null=True)
    nationality  = models.CharField(max_length=100,blank=True,null=True)
    marit_stat  = models.CharField(max_length=100,blank=True,null=True)

    insurance = models.CharField(max_length=100,blank=False,null=False)
    insurance_loc = models.CharField(max_length=100,blank=False,null=False)

    occupation = models.CharField(max_length=100,blank=True,null=True)
    other_occupation_bool = models.BooleanField()
    other_occupation = models.CharField(max_length=100,blank=True,null=True)
    main_occup_bool = models.BooleanField()
    main_occup = models.CharField(max_length=100,blank=True,null=True)
    beside_occup_bool = models.BooleanField()
    beside_occup = models.CharField(max_length=100,blank=True,null=True)
    beside_occup_income = models.CharField(max_length=100,blank=True,null=True)



    def __str__(self):
        return self.first_name+" "+self.last_name



    class Meta:
        ordering = ('created',)


# Create your models here.
