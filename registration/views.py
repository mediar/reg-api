from django.shortcuts import render
from registration.models import Registration
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.permissions import AllowAny


from registration.serializers import RegistrationSerializer


class RegistrationList(generics.ListCreateAPIView):
    queryset = Registration.objects.all()
    serializer_class = RegistrationSerializer
    permission_classes=(AllowAny, )

    def perform_create(self, serializer):
        serializer.save()
        return Response(serializer.data, status.HTTP_201_CREATED)


class RegistrationDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = RegistrationSerializer

    def get_queryset(self):
        return Registration.objects.all()

# Create your views here.
