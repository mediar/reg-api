from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from registration.models import Registration

class RegistrationSerializer(serializers.HyperlinkedModelSerializer):
    #user = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Registration
        exclude = ( )
        extra_kwargs = {
            'url':{
                'view_name': 'registration:registration-detail',
            }
        }
        validators = [
            UniqueTogetherValidator(
                queryset=Registration.objects.all(),
                fields=('first_name', 'last_name')
            )
        ]
